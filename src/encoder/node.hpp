#pragma once

#include <labrat/example-raspi/encoder/conversions.hpp>

#include <labrat/lbot/message.hpp>
#include <labrat/lbot/node.hpp>
#include <labrat/lbot/utils/thread.hpp>
#include <labrat/lbot/msg/foxglove/RawImage.hpp>

#include <linux/videodev2.h>

#include <atomic>

inline namespace labrat {
namespace example_raspi {

class EncoderNode : public lbot::Node {
public:
  /**
   * @brief Construct a new Encoder Node object
   *
   */
  EncoderNode();
  ~EncoderNode();

private:
  struct BufferInfo {
    void *start;
    int length;
    struct v4l2_buffer buffer;
    struct v4l2_plane plane;
  };

  void runOutput();
  void runCapture();

  void map(int fd, uint32_t type, BufferInfo *info);

  Sender<CompressedVideoMessage>::Ptr sender;
  Receiver<foxglove::RawImage>::Ptr receiver;

  int fd;

  struct BufferInfo output;
  struct BufferInfo capture;

  lbot::LoopThread output_thread;
  lbot::LoopThread capture_thread;
};

}}
