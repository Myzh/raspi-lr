#include <labrat/example-raspi/encoder/conversions.hpp>

#include <labrat/lbot/exception.hpp>

inline namespace labrat {
namespace example_raspi {

void CompressedVideoMessage::convertFrom(const BufferReference &source, Storage &destination) {
  destination.timestamp = std::make_unique<foxglove::Time>(std::chrono::duration_cast<std::chrono::seconds>(lbot::Clock::now().time_since_epoch()).count(), (std::chrono::duration_cast<std::chrono::nanoseconds>(lbot::Clock::now().time_since_epoch()) % std::chrono::seconds(1)).count());
  destination.frame_id = "enu";
  destination.format = "h264";

  destination.data.resize(source.size);
  memcpy(destination.data.data(), source.data, source.size);
}

}}
