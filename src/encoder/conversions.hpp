#pragma once

#include <labrat/lbot/message.hpp>
#include <labrat/lbot/msg/foxglove/CompressedVideo.hpp>

#include <cstdint>
#include <span>

inline namespace labrat {
namespace example_raspi {

struct BufferReference {
  void *data;
  int size;
};

class CompressedVideoMessage : public lbot::MessageBase<foxglove::CompressedVideo, BufferReference> {
public:
  static void convertFrom(const BufferReference &source, Storage &destination);
};

}}
