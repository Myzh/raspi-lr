#include <labrat/example-raspi/encoder/node.hpp>

#include <chrono>

#include <labrat/lbot/config.hpp>
#include <labrat/lbot/exception.hpp>

#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <cassert>
#include <cerrno>

/*
  https://lalitm.com/hw-encoding-raspi/
  https://www.kernel.org/doc/html/v6.1/userspace-api/media/v4l/dev-encoder.html
  https://www.kernel.org/doc/html/v4.9/media/uapi/v4l/extended-controls.html
*/

inline namespace labrat {
namespace example_raspi {

EncoderNode::EncoderNode() {
  lbot::Config::Ptr config = lbot::Config::get();

  sender = addSender<CompressedVideoMessage>("/encoder/compressed");
  receiver = addReceiver<foxglove::RawImage>("/camera/raw");

  // v4l2-ctl --list-devices
  fd = open("/dev/video11", O_RDWR);
  if (fd <= 0) {
    throw lbot::SystemException("Failed to open file descriptor", errno, getLogger());
  }

  struct v4l2_control ctrl;
  ctrl.id = V4L2_CID_MPEG_VIDEO_REPEAT_SEQ_HEADER;
  ctrl.value = 1;

  if (ioctl(fd, VIDIOC_S_CTRL, &ctrl)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_S_CTRL, &ctrl): ", errno, getLogger());
  }

  ctrl.id = V4L2_CID_MPEG_VIDEO_H264_I_PERIOD;
  ctrl.value = config->getParameterFallback("/encoder/i_frame_period", 30).get<int>();
  if (ioctl(fd, VIDIOC_S_CTRL, &ctrl)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_S_CTRL, &ctrl): ", errno, getLogger());
  }

  ctrl.id = V4L2_CID_MPEG_VIDEO_BITRATE;
  ctrl.value = config->getParameterFallback("/encoder/bitrate", 400000).get<int>();
  if (ioctl(fd, VIDIOC_S_CTRL, &ctrl)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_S_CTRL, &ctrl): ", errno, getLogger());
  }

  struct v4l2_format format;
  format.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
  if (ioctl(fd, VIDIOC_G_FMT, &format)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_G_FMT, &format): ", errno, getLogger());
  }

  format.fmt.pix_mp.width = config->getParameterFallback("/encoder/width", 640).get<int>();
  format.fmt.pix_mp.height = config->getParameterFallback("/encoder/width", 480).get<int>();
  format.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_YUYV;
  if (ioctl(fd, VIDIOC_S_FMT, &format)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_S_FMT, &format): ", errno, getLogger());
  }

  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
  if (ioctl(fd, VIDIOC_G_FMT, &format)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_G_FMT, &format): ", errno, getLogger());
  }

  format.fmt.pix_mp.width = config->getParameterFallback("/encoder/width", 640).get<int>();
  format.fmt.pix_mp.height = config->getParameterFallback("/encoder/width", 480).get<int>();
  if (ioctl(fd, VIDIOC_S_FMT, &format)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_S_FMT, &format): ", errno, getLogger());
  }

  struct v4l2_streamparm stream;
  memset(&stream, 0, sizeof(stream));
  stream.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
  stream.parm.output.timeperframe.numerator = 1;
  stream.parm.output.timeperframe.denominator = config->getParameterFallback("/encoder/framerate", 30L).get<int>();

  if (ioctl(fd, VIDIOC_S_PARM, &stream)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_S_PARM, &stream): ", errno, getLogger());
  }
    
  struct v4l2_requestbuffers request;
  request.memory = V4L2_MEMORY_MMAP;
  request.count = 1;
  request.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;

  if (ioctl(fd, VIDIOC_REQBUFS, &request)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_REQBUFS, &request): ", errno, getLogger());
  }

  map(fd, V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE, &output);

  request.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;

  if (ioctl(fd, VIDIOC_REQBUFS, &request)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_REQBUFS, &request): ", errno, getLogger());
  }

  map(fd, V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE, &capture);

  int type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
  if (ioctl(fd, VIDIOC_STREAMON, &type)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_STREAMON, &type): ", errno, getLogger());
  }

  type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
  if (ioctl(fd, VIDIOC_STREAMON, &type)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_STREAMON, &type): ", errno, getLogger());
  }

  output_thread = lbot::LoopThread(&EncoderNode::runOutput, "encoder out", 1, this);
  capture_thread = lbot::LoopThread(&EncoderNode::runCapture, "encoder cap", 1, this);
}

EncoderNode::~EncoderNode() {
  v4l2_encoder_cmd command;
  memset(&command, 0, sizeof(command));
  command.cmd = V4L2_ENC_CMD_STOP;

  if (ioctl(fd, VIDIOC_ENCODER_CMD, &command)) {
    getLogger().logError() << "ioctl failed (fd, VIDIOC_ENCODER_CMD, &command): " << errno;
  }

  // Join threads.
  capture_thread = lbot::LoopThread();
  output_thread = lbot::LoopThread();
  
  close(fd);
}

void EncoderNode::runOutput() {
  lbot::Message<foxglove::RawImage> input_frame;
  try {
    input_frame = receiver->next();
  } catch (lbot::TopicNoDataAvailableException &) {
    return;
  }

  struct v4l2_plane plane;
  memset(&plane, 0, sizeof(plane));

  struct v4l2_buffer buffer;
  buffer.memory = V4L2_MEMORY_MMAP;
  buffer.length = 1;
  buffer.m.planes = &plane;
  buffer.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
  
  assert(output.length == input_frame.data.size());
  memcpy(output.start, input_frame.data.data(), input_frame.data.size());
  output.plane.bytesused = input_frame.data.size();

  if (ioctl(fd, VIDIOC_QBUF, &output.buffer)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_QBUF, &output.buffer): ", errno, getLogger());
  }

  if (ioctl(fd, VIDIOC_DQBUF, &buffer)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_DQBUF, &buffer): ", errno, getLogger());
  }
}

void EncoderNode::runCapture() {
  struct v4l2_plane plane;
  memset(&plane, 0, sizeof(plane));

  struct v4l2_buffer buffer;
  buffer.memory = V4L2_MEMORY_MMAP;
  buffer.length = 1;
  buffer.m.planes = &plane;
  buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;

  if (ioctl(fd, VIDIOC_QBUF, &capture.buffer)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_QBUF, &capture.buffer): ", errno, getLogger());
  }

  if (ioctl(fd, VIDIOC_DQBUF, &buffer)) {
    if (errno == EPIPE) {
      getLogger().logWarning() << "Read beyond the last capture frame";
      return;
    }

    throw lbot::SystemException("ioctl failed (fd, VIDIOC_DQBUF, &buffer): ", errno, getLogger());
  }

  if (!(buffer.flags & V4L2_BUF_FLAG_LAST)) {
    BufferReference ref;
    ref.data = capture.start;
    ref.size = buffer.m.planes[0].bytesused;
    sender->put(ref);
  }
}

void EncoderNode::map(int fd, uint32_t type, struct BufferInfo* info) {
  struct v4l2_buffer *buffer = &info->buffer;

  memset(buffer, 0, sizeof(*buffer));
  buffer->type = type;
  buffer->memory = V4L2_MEMORY_MMAP;
  buffer->index = 0;
  buffer->length = 1;
  buffer->m.planes = &info->plane;

  if (ioctl(fd, VIDIOC_QUERYBUF, buffer)) {
    throw lbot::SystemException("ioctl failed (fd, VIDIOC_QUERYBUF, buffer): ", errno, getLogger());
  }

  info->length = buffer->m.planes[0].length;
  info->start = mmap(NULL, info->length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buffer->m.planes[0].m.mem_offset);
}

}}
