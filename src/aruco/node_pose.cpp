#include <labrat/example-raspi/aruco/node_pose.hpp>

#include <chrono>
#include <iostream>

#include <labrat/lbot/exception.hpp>
#include <labrat/lbot/config.hpp>
#include <labrat/lbot/utils/thread.hpp>

inline namespace labrat {
namespace example_raspi {

ArucoPoseNode::ArucoPoseNode(){
  lbot::Config::Ptr config = lbot::Config::get();

  sender_poses = addSender<PosesInFrameMessage>("/aruco/poses");
  sender_calibration = addSender<CameraCalibrationMessage>("/aruco/calibration");

  receiver_corners = addReceiver<ImageAnnotationsMessage>("/aruco/corners");

  calibration_parameters.camera = cv::Mat1d::zeros(3, 3);
  calibration_parameters.camera.at<double>(0, 0) = config->getParameter("/calibration/camera/fx").get<double>();
  calibration_parameters.camera.at<double>(0, 2) = config->getParameter("/calibration/camera/cx").get<double>();
  calibration_parameters.camera.at<double>(1, 1) = config->getParameter("/calibration/camera/fy").get<double>();
  calibration_parameters.camera.at<double>(1, 2) = config->getParameter("/calibration/camera/cy").get<double>();
  calibration_parameters.camera.at<double>(2, 2) = 1;

  calibration_parameters.distortion = cv::Mat1d::zeros(1, 14);
  calibration_parameters.distortion.at<double>(0) = config->getParameter("/calibration/distortion/k1").get<double>();
  calibration_parameters.distortion.at<double>(1) = config->getParameter("/calibration/distortion/k2").get<double>();
  calibration_parameters.distortion.at<double>(2) = config->getParameterFallback("/calibration/distortion/p1", 0).get<double>();
  calibration_parameters.distortion.at<double>(3) = config->getParameterFallback("/calibration/distortion/p2", 0).get<double>();
  calibration_parameters.distortion.at<double>(4) = config->getParameter("/calibration/distortion/k3").get<double>();
  calibration_parameters.distortion.at<double>(5) = config->getParameter("/calibration/distortion/k4").get<double>();
  calibration_parameters.distortion.at<double>(6) = config->getParameter("/calibration/distortion/k5").get<double>();
  calibration_parameters.distortion.at<double>(7) = config->getParameter("/calibration/distortion/k6").get<double>();
  calibration_parameters.distortion.at<double>(8) = config->getParameterFallback("/calibration/distortion/s1", 0).get<double>();
  calibration_parameters.distortion.at<double>(9) = config->getParameterFallback("/calibration/distortion/s2", 0).get<double>();
  calibration_parameters.distortion.at<double>(10) = config->getParameterFallback("/calibration/distortion/s3", 0).get<double>();
  calibration_parameters.distortion.at<double>(11) = config->getParameterFallback("/calibration/distortion/s4", 0).get<double>();
  calibration_parameters.distortion.at<double>(12) = config->getParameterFallback("/calibration/distortion/t1", 0).get<double>();
  calibration_parameters.distortion.at<double>(13) = config->getParameterFallback("/calibration/distortion/t2", 0).get<double>();

  calibration_parameters.size.width = config->getParameter("/calibration/width").get<int>();
  calibration_parameters.size.height = config->getParameter("/calibration/height").get<int>();

  thread_pose = lbot::LoopThread(&ArucoPoseNode::runPose, "aruco pose", 1, this);
  thread_calibration = lbot::TimerThread(&ArucoPoseNode::runCalibration, std::chrono::seconds(1), "aruco calib", 1, this);
}

void ArucoPoseNode::runPose() {
  lbot::Config::Ptr config = lbot::Config::get();
  std::vector<std::vector<cv::Point2f>> corners;

  try {
    corners = receiver_corners->next();
  } catch (lbot::TopicNoDataAvailableException &) {
    lbot::Thread::sleepFor(std::chrono::seconds(1));
    return;
  }

  if (corners.empty()) {
    return;
  }

  PoseVectors poses;
  cv::aruco::estimatePoseSingleMarkers(corners, config->getParameterFallback("/aruco/marker_size", 0.1).get<double>(), calibration_parameters.camera, calibration_parameters.distortion, poses.rotation, poses.translation);

  sender_poses->put(poses);
}

void ArucoPoseNode::runCalibration() {
  sender_calibration->put(calibration_parameters);
}

}}
