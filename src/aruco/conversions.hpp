#pragma once

#include <labrat/lbot/message.hpp>
#include <labrat/lbot/msg/foxglove/RawImage.hpp>
#include <labrat/lbot/msg/foxglove/ImageAnnotations.hpp>
#include <labrat/lbot/msg/foxglove/PosesInFrame.hpp>
#include <labrat/lbot/msg/foxglove/CameraCalibration.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>

#include <vector>

inline namespace labrat {
namespace example_raspi {

struct PoseVectors {
  std::vector<cv::Vec3d> translation;
  std::vector<cv::Vec3d> rotation;
};

struct CalibrationParameters {
  cv::Mat1d camera;
  cv::Mat1d distortion;
  cv::Size size;
};

class ImageAnnotationsMessage : public lbot::MessageBase<foxglove::ImageAnnotations, std::vector<std::vector<cv::Point2f>>> {
public:
  static void convertFrom(const std::vector<std::vector<cv::Point2f>> &source, Storage &destination);
  static void convertTo(const Storage &source, std::vector<std::vector<cv::Point2f>> &destination);
};

class RawImageMessage : public lbot::MessageBase<foxglove::RawImage, cv::Mat1b> {
public:
  static void convertTo(const Storage &source, cv::Mat1b &destination);
  static void moveTo(Storage &&source, cv::Mat1b &destination);
};

class PosesInFrameMessage : public lbot::MessageBase<foxglove::PosesInFrame, PoseVectors> {
public:
  static void convertFrom(const PoseVectors &source, Storage &destination);
};

class CameraCalibrationMessage : public lbot::MessageBase<foxglove::CameraCalibration, CalibrationParameters> {
public:
  static void convertFrom(const CalibrationParameters &source, Storage &destination);
};

}}
