#pragma once

#include <labrat/example-raspi/aruco/conversions.hpp>

#include <labrat/lbot/node.hpp>
#include <labrat/lbot/utils/thread.hpp>

#include <atomic>
#include <iostream>

inline namespace labrat {
namespace example_raspi {

class ArucoCornerNode : public lbot::Node {
public:
  /**
   * @brief Construct a new Camera Node object
   *
   */
  ArucoCornerNode();
  ~ArucoCornerNode();

private:
  void run();

  Sender<ImageAnnotationsMessage>::Ptr sender_corners;
  Sender<ImageAnnotationsMessage>::Ptr sender_rejected;

  Receiver<RawImageMessage>::Ptr receiver_image;

  cv::aruco::ArucoDetector detector;

  lbot::LoopThread thread;
};

}}
