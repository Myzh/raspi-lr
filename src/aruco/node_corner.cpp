#include <labrat/example-raspi/aruco/node_corner.hpp>

#include <chrono>

#include <labrat/lbot/exception.hpp>

inline namespace labrat {
namespace example_raspi {

ArucoCornerNode::ArucoCornerNode(){
  sender_corners = addSender<ImageAnnotationsMessage>("/aruco/corners");
  sender_rejected = addSender<ImageAnnotationsMessage>("/aruco/rejected");

  receiver_image = addReceiver<RawImageMessage>("/camera/raw");

  const cv::aruco::DetectorParameters detector_parameter;
  const cv::aruco::Dictionary dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_50);
  detector = cv::aruco::ArucoDetector(dictionary, detector_parameter);

  thread = lbot::LoopThread(&ArucoCornerNode::run, "aruco corner", 1,  this);
}

ArucoCornerNode::~ArucoCornerNode() = default;

void ArucoCornerNode::run() {
  cv::Mat1b image;

  try {
    image = receiver_image->next();
  } catch (lbot::TopicNoDataAvailableException &) {
    lbot::Thread::sleepFor(std::chrono::seconds(1));
    return;
  }

  std::vector<int> ids;
  std::vector<std::vector<cv::Point2f>> corners;
  std::vector<std::vector<cv::Point2f>> rejected;

  detector.detectMarkers(image, corners, ids, rejected);

  sender_corners->put(corners);
  sender_rejected->put(rejected);
}

}}
