#pragma once

#include <labrat/example-raspi/aruco/conversions.hpp>

#include <labrat/lbot/node.hpp>
#include <labrat/lbot/utils/thread.hpp>

inline namespace labrat {
namespace example_raspi {

class ArucoPoseNode : public lbot::Node {
public:
  /**
   * @brief Construct a new Camera Node object
   *
   */
  ArucoPoseNode();
  ~ArucoPoseNode() = default;

private:
  void runPose();
  void runCalibration();

  Sender<PosesInFrameMessage>::Ptr sender_poses;
  Sender<CameraCalibrationMessage>::Ptr sender_calibration;

  Receiver<ImageAnnotationsMessage>::Ptr receiver_corners;

  CalibrationParameters calibration_parameters;

  lbot::LoopThread thread_pose;
  lbot::TimerThread thread_calibration;
};

}}
