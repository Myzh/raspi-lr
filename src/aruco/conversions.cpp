#include <labrat/example-raspi/aruco/conversions.hpp>

#include <chrono>

#include <labrat/lbot/exception.hpp>

inline namespace labrat {
namespace example_raspi {

void ImageAnnotationsMessage::convertFrom(const std::vector<std::vector<cv::Point2f>> &source, Storage &destination) {
  const foxglove::Time timestamp(std::chrono::duration_cast<std::chrono::seconds>(lbot::Clock::now().time_since_epoch()).count(), (std::chrono::duration_cast<std::chrono::nanoseconds>(lbot::Clock::now().time_since_epoch()) % std::chrono::seconds(1)).count());

  destination.points.clear();
  destination.points.reserve(source.size());

  const foxglove::ColorNative color{.r = 0x00, .g = 0xff, .b = 0x00, .a = 0x80};
  
  for (const std::vector<cv::Point2f> &marker : source) {
    std::unique_ptr<foxglove::PointsAnnotationNative> &annotation = destination.points.emplace_back(std::make_unique<foxglove::PointsAnnotationNative>());

    annotation->timestamp = std::make_unique<foxglove::Time>(timestamp);
    annotation->type = foxglove::PointsAnnotationType::LINE_LOOP;

    annotation->points.clear();
    annotation->points.reserve(marker.size());
    annotation->outline_colors.clear();
    annotation->outline_colors.reserve(marker.size());

    for (const cv::Point2f &corner : marker) {
      std::unique_ptr<foxglove::Point2Native> &point = annotation->points.emplace_back(std::make_unique<foxglove::Point2Native>());
      annotation->outline_colors.push_back(std::make_unique<foxglove::ColorNative>(color));

      point->x = corner.x;
      point->y = corner.y;
    }

    annotation->thickness = 5;
  }
}

void ImageAnnotationsMessage::convertTo(const Storage &source, std::vector<std::vector<cv::Point2f>> &destination) {
  destination.clear();
  destination.reserve(source.points.size());

  for (const std::unique_ptr<foxglove::PointsAnnotationNative> &annotation : source.points) {
    std::vector<cv::Point2f> &marker = destination.emplace_back();

    for (const std::unique_ptr<foxglove::Point2Native> &point : annotation->points) {
      cv::Point2f &corner = marker.emplace_back();

      corner.x = point->x;
      corner.y = point->y;
    }
  }
}

void RawImageMessage::convertTo(const Storage &source, cv::Mat1b &destination) {
  if (source.encoding != "yuyv") {
    throw lbot::IoException("Frame encoding unsupported.");
  }

  cv::Mat image = cv::Mat1b(source.data, true).reshape(2, source.height);
  cv::cvtColor(image, destination, cv::COLOR_YUV2GRAY_YUYV);
}

void RawImageMessage::moveTo(Storage &&source, cv::Mat1b &destination) {
  if (source.encoding != "yuyv") {
    throw lbot::IoException("Frame encoding unsupported.");
  }

  cv::Mat image = cv::Mat1b(source.data, false).reshape(2, source.height);
  cv::cvtColor(image, destination, cv::COLOR_YUV2GRAY_YUYV);
}

void PosesInFrameMessage::convertFrom(const PoseVectors &source, Storage &destination) {
  destination.timestamp = std::make_unique<foxglove::Time>(std::chrono::duration_cast<std::chrono::seconds>(lbot::Clock::now().time_since_epoch()).count(), (std::chrono::duration_cast<std::chrono::nanoseconds>(lbot::Clock::now().time_since_epoch()) % std::chrono::seconds(1)).count());
  destination.frame_id = "enu";

  if (source.translation.size() != source.rotation.size() ) {
    throw lbot::IoException("Translation and rotation vectors do not have the same size.");
  }

  const std::size_t size = source.translation.size();

  destination.poses.clear();
  destination.poses.reserve(size);

  for (std::size_t i = 0; i < size; ++i) {
    const cv::Vec3d &translation = source.translation[i];
    const cv::Vec3d &rotation = source.rotation[i];

    std::unique_ptr<foxglove::PoseNative> &pose = destination.poses.emplace_back(std::make_unique<foxglove::PoseNative>());
    
    pose->position = std::make_unique<foxglove::Vector3Native>();
    pose->position->x = translation[0];
    pose->position->y = translation[1];
    pose->position->z = translation[2];

    double angle = std::sqrt(std::pow(rotation[0], 2) + std::pow(rotation[1], 2) + std::pow(rotation[2], 2));
    double angle_2 = angle / 2;

    pose->orientation = std::make_unique<foxglove::QuaternionNative>();
    pose->orientation->x = rotation[0] / angle * std::sin(angle_2);
    pose->orientation->y = rotation[1] / angle * std::sin(angle_2);
    pose->orientation->z = rotation[2] / angle * std::sin(angle_2);
    pose->orientation->w = std::cos(angle_2);
  }
}

void CameraCalibrationMessage::convertFrom(const CalibrationParameters &source, Storage &destination) {
  destination.timestamp = std::make_unique<foxglove::Time>(std::chrono::duration_cast<std::chrono::seconds>(lbot::Clock::now().time_since_epoch()).count(), (std::chrono::duration_cast<std::chrono::nanoseconds>(lbot::Clock::now().time_since_epoch()) % std::chrono::seconds(1)).count());
  destination.frame_id = "enu";
  destination.distortion_model = "plumb_bob";

  destination.width = source.size.width;
  destination.height = source.size.height;

  {
    const int size = source.distortion.size().width;

    destination.d.clear();
    destination.d.reserve(size);

    for (int i = 0; i < size; ++i) {
      destination.d.emplace_back(source.distortion.at<double>(0, i));
    }
  }

  {
    destination.k.clear();
    destination.k.reserve(9);
    destination.p.clear();
    destination.p.reserve(12);

    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) {
        destination.k.emplace_back(source.camera.at<double>(i, j));
        destination.p.emplace_back(source.camera.at<double>(i, j));
      }
      destination.p.emplace_back(0);
    }
  }

  destination.r = {1, 0, 0, 0, 1, 0, 0, 0, 1};
}

}}
