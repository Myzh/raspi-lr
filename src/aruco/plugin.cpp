#include <labrat/example-raspi/aruco/plugin.hpp>
#include <labrat/example-raspi/aruco/node_corner.hpp>
#include <labrat/example-raspi/aruco/node_pose.hpp>


inline namespace labrat {
namespace example_raspi {

ArucoPlugin::ArucoPlugin() {
  addNode<ArucoCornerNode>("aruco_corner");
  addNode<ArucoPoseNode>("aruco_pose");
}

}}
