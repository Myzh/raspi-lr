#include <labrat/example-raspi/camera/node.hpp>

#include <chrono>

#include <labrat/lbot/exception.hpp>
#include <opencv2/imgcodecs.hpp>

inline namespace labrat {
namespace example_raspi {

CameraNode::CameraNode() {
  sender_raw = addSender<RawImageMessage>("/camera/raw");

  video_capture = cv::VideoCapture(0);
  video_capture.set(cv::CAP_PROP_BUFFERSIZE, 1);
  video_capture.set(cv::CAP_PROP_CONVERT_RGB, false);
  video_capture.set(cv::CAP_PROP_FOURCC, cv::VideoWriter::fourcc('Y', 'U', 'Y', 'V'));

  if (!video_capture.isOpened()) {
    throw lbot::IoException("No video stream detected.");
  }

  getLogger().logInfo() << "Using video backend: " << video_capture.getBackendName();

  thread_raw = lbot::LoopThread(&CameraNode::runRaw, "camera raw", 1, this);
}

CameraNode::~CameraNode() = default;

void CameraNode::runRaw() {
  cv::Mat1b image;

  try {
    video_capture >> image;
  } catch (cv::Exception &e) {
    getLogger().logWarning() << "Exception thrown while capturing frame: " << e.what();
    return;
  }

  if (image.empty()) {
    getLogger().logWarning() << "Empty video frame captured.";
    return;
  }

  sender_raw->put(image);
}

}}
