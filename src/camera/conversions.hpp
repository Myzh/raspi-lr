#pragma once

#include <labrat/lbot/message.hpp>
#include <labrat/lbot/msg/foxglove/RawImage.hpp>

#include <opencv2/opencv.hpp>

inline namespace labrat {
namespace example_raspi {

class RawImageMessage : public lbot::MessageBase<foxglove::RawImage, cv::Mat1b> {
public:
  static void convertFrom(const cv::Mat1b &source, Storage &destination);
};

}}
