#pragma once

#include <labrat/example-raspi/camera/conversions.hpp>

#include <labrat/lbot/message.hpp>
#include <labrat/lbot/node.hpp>
#include <labrat/lbot/utils/thread.hpp>

#include <opencv2/opencv.hpp>

#include <mutex>

inline namespace labrat {
namespace example_raspi {

class CameraNode : public lbot::Node {
public:
  /**
   * @brief Construct a new Camera Node object
   *
   */
  CameraNode();
  ~CameraNode();

private:
  void runRaw();

  Sender<RawImageMessage>::Ptr sender_raw;

  cv::VideoCapture video_capture;

  lbot::LoopThread thread_raw;
};

}}
