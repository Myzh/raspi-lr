#include <labrat/example-raspi/camera/conversions.hpp>

#include <labrat/lbot/exception.hpp>

inline namespace labrat {
namespace example_raspi {

void RawImageMessage::convertFrom(const cv::Mat1b &source, Storage &destination) {
  destination.timestamp = std::make_unique<foxglove::Time>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count(), (std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()) % std::chrono::seconds(1)).count());

  if (!source.isContinuous()) {
    throw lbot::IoException("Image matrix is not continuous.");
  }

  destination.frame_id = "enu";
  destination.encoding = "yuyv";

  destination.width = source.size().width;
  destination.height = source.size().height;
  destination.step = source.step;

  //destination.data.assign(source.data, source.data + source.total());
  const int size = source.dataend - source.data;
  destination.data.resize(size);
  memcpy(destination.data.data(), source.data, size);
}

}}
