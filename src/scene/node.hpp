#pragma once

#include <labrat/lbot/msg/foxglove/SceneUpdate.hpp>

#include <labrat/lbot/message.hpp>
#include <labrat/lbot/node.hpp>
#include <labrat/lbot/utils/thread.hpp>

#include <vector>

inline namespace labrat {
namespace example_raspi {

class SceneNode : public lbot::Node {
public:
  /**
   * @brief Construct a new Scene Node object
   *
   */
  SceneNode();
  ~SceneNode();

private:
  void run();

  Sender<foxglove::SceneUpdate>::Ptr sender;

  std::vector<uint8_t> asset_data;

  lbot::TimerThread thread;
};

}}
