#include <labrat/example-raspi/scene/node.hpp>

#include <chrono>
#include <vector>
#include <string>
#include <fstream>

#include <labrat/lbot/exception.hpp>
#include <labrat/lbot/config.hpp>

inline namespace labrat {
namespace example_raspi {

std::vector<uint8_t> readFile(const std::string &path) {
  std::ifstream file(path, std::ios::binary);

  if (!file) {
    throw lbot::IoException("Model " + path + " not found");
  }

  file.unsetf(std::ios::skipws);

  return std::vector<uint8_t>(std::istream_iterator<uint8_t>(file), std::istream_iterator<uint8_t>());
}

SceneNode::SceneNode() {
  lbot::Config::Ptr config = lbot::Config::get();

  sender = addSender<foxglove::SceneUpdate>("/scene");

  asset_data = readFile(config->getParameterFallback("/scene/asset_file", "assets/duck.glb").get<std::string>());

  thread = lbot::TimerThread(&SceneNode::run, std::chrono::seconds(10), "scene", 1, this);
}

SceneNode::~SceneNode() = default;

void SceneNode::run() {
  lbot::Message<foxglove::SceneUpdate> message;

  std::unique_ptr<foxglove::SceneEntityNative> entity = std::make_unique<foxglove::SceneEntityNative>();

  entity->timestamp = std::make_unique<foxglove::Time>(std::chrono::duration_cast<std::chrono::seconds>(lbot::Clock::now().time_since_epoch()).count(), 0);
  entity->frame_id = "raspi";
  entity->id = "Raspberry Pi";
  entity->lifetime = 0;
  entity->frame_locked = true;

  std::unique_ptr<foxglove::ModelPrimitiveNative> model = std::make_unique<foxglove::ModelPrimitiveNative>();

  model->scale = std::make_unique<foxglove::Vector3Native>();
  model->scale->x = 1;
  model->scale->y = 1;
  model->scale->z = 1;
  model->override_color = false;
  model->media_type = "model/gltf-binary";
  model->data = asset_data;

  entity->models.emplace_back(std::move(model));

  message.entities.emplace_back(std::move(entity));

  sender->put(message);
}

}}
