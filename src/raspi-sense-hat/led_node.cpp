#include <labrat/example-raspi/raspi-sense-hat/led_node.hpp>
#include <labrat/lbot/exception.hpp>

#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/fb.h>
#include <unistd.h>

inline namespace labrat {
namespace example_raspi {

RaspiLedNode::RaspiLedNode(){
  file_descriptor = openFramebuffer();
  framebuffer = reinterpret_cast<FramebufferPixel *>(mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_SHARED, file_descriptor, 0));

  if (framebuffer == (void *)-1) {
    throw lbot::IoException("Failed to mmap frame buffer", errno);
  }
}

RaspiLedNode::~RaspiLedNode() {
  munmap(framebuffer, size);

  close(file_descriptor);
}

std::size_t RaspiLedNode::openFramebuffer() {
  for (std::size_t i = 0; i < max_files; ++i) {
    const std::string filename = "/dev/fb" + std::to_string(i);

    std::size_t result = open(filename.c_str(), O_RDWR);

    if (result < 0) {
      if (errno == ENOENT) {
        throw lbot::IoException("No frame buffer found");
      }
    }

    fb_fix_screeninfo info;
    if (ioctl(result, FBIOGET_FSCREENINFO, &info) == 0) {
      if (std::string(info.id) == "RPi-Sense FB") {
        return result;
      }
    }

    close(result);
  }

  throw lbot::IoException("No frame buffer found within allowed index range");
}

}}
