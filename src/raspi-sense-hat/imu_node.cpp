#include <labrat/example-raspi/raspi-sense-hat/imu_node.hpp>
#include <labrat/lbot/exception.hpp>

#include <ratio>
#include <cmath>

#include <errno.h>

inline namespace labrat {
namespace example_raspi {

RaspiImuNode::RaspiImuNode(){
  sender_pose = addSender<PoseMessage>("/raspi_sense_hat/imu_pose");
  sender_accelerometer = addSender<AccelerationMessage>("/raspi_sense_hat/imu_accelerometer");
  sender_gyroscope = addSender<GyroscopeMessage>("/raspi_sense_hat/imu_gyroscope");
  sender_compass = addSender<CompassMessage>("/raspi_sense_hat/imu_compass");
  sender_environment = addSender<EnvironmentMessage>("/raspi_sense_hat/imu_environment");

  sender_frame = addSender<foxglove::FrameTransform>("/raspi_sense_hat/frame");

  settings = new RTIMUSettings("RTIMULib");
  imu = RTIMU::createIMU(settings);
  pressure = RTPressure::createPressure(settings);
  humidity = RTHumidity::createHumidity(settings);

  if ((imu == nullptr) || (imu->IMUType() == RTIMU_TYPE_NULL)) {
    throw lbot::IoException("No IMU found");
  }

  imu->IMUInit();

  imu->setSlerpPower(0.02);
  imu->setGyroEnable(true);
  imu->setAccelEnable(true);
  imu->setCompassEnable(true);

  if (pressure == nullptr) {
    throw lbot::IoException("No pressure sensor found");
  }

  if (humidity == nullptr) {
    throw lbot::IoException("No humidity sensor found");
  }

  humidity->humidityInit();
  pressure->pressureInit();

  imu_thread = lbot::TimerThread(&RaspiImuNode::readImu, std::chrono::milliseconds(imu->IMUGetPollInterval()), "raspi imu", 1, this);
  frame_thread = lbot::TimerThread(&RaspiImuNode::sendFrame, std::chrono::seconds(1), "raspi frame", 1, this);
}

RaspiImuNode::~RaspiImuNode() {}

void RaspiImuNode::readImu() {
  if (!imu->IMURead()) {
    return;
  }

  RTIMU_DATA imu_data = imu->getIMUData();

  pressure->pressureRead(imu_data);
  humidity->humidityRead(imu_data);

  sender_pose->put(imu_data);
  sender_accelerometer->put(imu_data);
  sender_gyroscope->put(imu_data);
  sender_compass->put(imu_data);
  sender_environment->put(imu_data);
}

void RaspiImuNode::sendFrame() {
  lbot::Message<foxglove::FrameTransform> message;

  message.timestamp = std::make_unique<foxglove::Time>(std::chrono::duration_cast<std::chrono::seconds>(lbot::Clock::now().time_since_epoch()).count(), 0);
  message.parent_frame_id = "enu";
  message.child_frame_id = "ned";

  message.rotation = std::make_unique<foxglove::QuaternionNative>();
  message.rotation->x = -std::sqrt(2.0) / 2.0;
  message.rotation->y = -std::sqrt(2.0) / 2.0;
  message.rotation->z = 0;
  message.rotation->w = 0;

  sender_frame->put(message);
}

}}
