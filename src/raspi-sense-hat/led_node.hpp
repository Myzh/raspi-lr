#pragma once

#include <labrat/lbot/message.hpp>
#include <labrat/lbot/node.hpp>

inline namespace labrat {
namespace example_raspi {

struct FramebufferPixel {
  uint16_t red : 5;
  uint16_t green : 6;
  uint16_t blue : 5;
};

static_assert(sizeof(FramebufferPixel) == sizeof(uint16_t));

class RaspiLedNode : public lbot::Node {
public:
  /**
   * @brief Construct a new Raspi Sense Hat Node object
   *
   */
  RaspiLedNode();
  ~RaspiLedNode();

  inline bool framebufferReady() {
    return (framebuffer != nullptr);
  }

  inline FramebufferPixel &getPixel(std::size_t x, std::size_t y) {
    const std::size_t index = x + y * dimension;

    return framebuffer[index];
  }

private:
  static std::size_t openFramebuffer();

  FramebufferPixel *framebuffer = nullptr;

  sigset_t signal_mask;

  std::size_t file_descriptor;

  static constexpr std::size_t dimension = 8;
  static constexpr std::size_t size = dimension * dimension;

  static constexpr std::size_t max_files = 100;
  static constexpr std::size_t timeout = 1000;
};

}}
