#pragma once

#include <labrat/example-raspi/raspi-sense-hat/conversions.hpp>

#include <labrat/lbot/node.hpp>
#include <labrat/lbot/utils/thread.hpp>
#include <labrat/lbot/msg/foxglove/FrameTransform.hpp>

inline namespace labrat {
namespace example_raspi {

class RaspiImuNode : public lbot::Node {
public:
  /**
   * @brief Construct a new Raspi Sense Hat Node object
   *
   */
  RaspiImuNode();
  ~RaspiImuNode();

private:
  void readImu();
  void sendFrame();

  Sender<PoseMessage>::Ptr sender_pose;
  Sender<AccelerationMessage>::Ptr sender_accelerometer;
  Sender<GyroscopeMessage>::Ptr sender_gyroscope;
  Sender<CompassMessage>::Ptr sender_compass;
  Sender<EnvironmentMessage>::Ptr sender_environment;
  Sender<foxglove::FrameTransform>::Ptr sender_frame;

  lbot::TimerThread imu_thread;
  lbot::TimerThread frame_thread;

  RTIMUSettings *settings;
  RTIMU *imu;
  RTPressure *pressure;
  RTHumidity *humidity;
};

}}
