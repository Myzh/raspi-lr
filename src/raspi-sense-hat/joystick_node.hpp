#pragma once

#include <labrat/example-raspi/raspi-sense-hat/conversions.hpp>

#include <labrat/lbot/node.hpp>
#include <labrat/lbot/utils/thread.hpp>

#include <signal.h>

inline namespace labrat {
namespace example_raspi {

class RaspiJoystickNode : public lbot::Node {
public:
  /**
   * @brief Construct a new Raspi Sense Hat Node object
   *
   */
  RaspiJoystickNode();
  ~RaspiJoystickNode();

private:
  static std::size_t openJoystick();

  void readJoystick();

  Sender<JoystickInputMessage>::Ptr sender;

  lbot::LoopThread thread;

  sigset_t signal_mask;

  std::size_t file_descriptor;
  ssize_t epoll_handle;

  static constexpr std::size_t max_files = 100;
  static constexpr std::size_t timeout = 1000;
};

}}
