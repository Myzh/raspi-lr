#pragma once

#include <labrat/lbot/message.hpp>
#include <labrat/example/msg/joystick_input.hpp>
#include <labrat/lbot/msg/foxglove/FrameTransform.hpp>
#include <labrat/lbot/msg/foxglove/Vector3.hpp>
#include <labrat/example/msg/environment.hpp>

#include <RTIMULib.h>

inline namespace labrat {
namespace example_raspi {

enum class KeyCode {
  none = 0,
  press = 28,
  up = 103,
  left = 105,
  right = 106,
  down = 108,
};

class JoystickInputMessage : public lbot::MessageBase<example::msg::JoystickInput, KeyCode> {
public:
  static void convertFrom(const KeyCode &source, Storage &destination);
};

class PoseMessage : public lbot::MessageBase<foxglove::FrameTransform, RTIMU_DATA> {
public:
  static void convertFrom(const RTIMU_DATA &source, Storage &destination);
};

class AccelerationMessage : public lbot::MessageBase<foxglove::Vector3, RTIMU_DATA> {
public:
  static void convertFrom(const RTIMU_DATA &source, Storage &destination);
};

class GyroscopeMessage : public lbot::MessageBase<foxglove::Vector3, RTIMU_DATA> {
public:
  static void convertFrom(const RTIMU_DATA &source, Storage &destination);
};

class CompassMessage : public lbot::MessageBase<foxglove::Vector3, RTIMU_DATA> {
public:
  static void convertFrom(const RTIMU_DATA &source, Storage &destination);
};

class EnvironmentMessage : public lbot::MessageBase<example::msg::Environment, RTIMU_DATA> {
public:
  static void convertFrom(const RTIMU_DATA &source, Storage &destination);
};

}}
