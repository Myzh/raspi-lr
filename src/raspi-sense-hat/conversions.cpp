#include <labrat/example-raspi/raspi-sense-hat/conversions.hpp>

#include <labrat/lbot/exception.hpp>

inline namespace labrat {
namespace example_raspi {

void JoystickInputMessage::convertFrom(const KeyCode &source, Storage &destination) {
  switch (source) {
    case (KeyCode::press): {
      destination.code = example::msg::Keycode::press;
      break;
    }
    case (KeyCode::up): {
      destination.code = example::msg::Keycode::up;
      break;
    }
    case (KeyCode::left): {
      destination.code = example::msg::Keycode::left;
      break;
    }
    case (KeyCode::right): {
      destination.code = example::msg::Keycode::right;
      break;
    }
    case (KeyCode::down): {
      destination.code = example::msg::Keycode::down;
      break;
    }
    default: {
      destination.code = example::msg::Keycode::none;
    }
  }
}

void PoseMessage::convertFrom(const RTIMU_DATA &source, Storage &destination) {
  destination.timestamp = std::make_unique<foxglove::Time>(source.timestamp / std::micro::den, (source.timestamp % std::micro::den) * (std::nano::den / std::micro::den));
  destination.parent_frame_id = "enu";
  destination.child_frame_id = "raspi";
 
  if (source.fusionQPoseValid) {
    destination.rotation = std::make_unique<foxglove::QuaternionNative>();
    destination.rotation->x = source.fusionQPose.x();
    destination.rotation->y = source.fusionQPose.y();
    destination.rotation->z = source.fusionQPose.z();
    destination.rotation->w = source.fusionQPose.scalar();
  }
}

void AccelerationMessage::convertFrom(const RTIMU_DATA &source, Storage &destination) {
  if (source.accelValid) {
    destination.x = source.accel.x();
    destination.y = source.accel.y();
    destination.z = source.accel.z();
  }
}

void GyroscopeMessage::convertFrom(const RTIMU_DATA &source, Storage &destination) {
  if (source.gyroValid) {
    destination.x = source.gyro.x();
    destination.y = source.gyro.y();
    destination.z = source.gyro.z();
  }
}

void CompassMessage::convertFrom(const RTIMU_DATA &source, Storage &destination) {
  if (source.compassValid) {
    destination.x = source.compass.x();
    destination.y = source.compass.y();
    destination.z = source.compass.z();
  }
}

void EnvironmentMessage::convertFrom(const RTIMU_DATA &source, Storage &destination) {
  if (source.temperatureValid) {
    destination.temperature = source.temperature;
  }

  if (source.pressureValid) {
    destination.pressure = source.pressure;
  }

  if (source.humidityValid) {
    destination.humidity = source.humidity;
  }
}

}}
