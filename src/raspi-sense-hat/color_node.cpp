#include <labrat/example-raspi/raspi-sense-hat/color_node.hpp>
#include <labrat/lbot/exception.hpp>
#include <labrat/lbot/utils/thread.hpp>

#include <chrono>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <endian.h>
#include <errno.h>
#include <linux/i2c-dev.h>

extern "C" {
#include <i2c/smbus.h>
}

// Data sheet: https://ams.com/tcs34725#tab/documents

inline namespace labrat {
namespace example_raspi {

RaspiColorNode::RaspiColorNode(){
  sender = addSender<example::msg::Color>("/raspi_sense_hat/color");

  file_descriptor = openColorSensor();
  setEnabled(true);
  setGain(3);
  setIntegrationCycles(200);

  thread = lbot::TimerThread(&RaspiColorNode::readColor, std::chrono::milliseconds(250), "raspi color", 1, this);
}

RaspiColorNode::~RaspiColorNode() {
  setEnabled(false);

  close(file_descriptor);
}

std::size_t RaspiColorNode::openColorSensor() {
  std::size_t result = open("/dev/i2c-1", O_RDWR);

  if (result < 0) {
    throw lbot::IoException("Failed to open color sensor", errno);
  }

  static constexpr uint8_t address = 0x29;
  
  // Set I2C slave address.
  if (ioctl(result, I2C_SLAVE, address) < 0) {
    throw lbot::IoException("Failed to set I2C slave address for color sensor", errno);
  }

  return result;
}

void RaspiColorNode::readColor() {
  lbot::Message<example::msg::Color> message;

  if (!(getEnabled() && getValid())) {
    return;
  }

  RawColor color = getColor();
  message.clear = color.clear;
  message.clear = color.red;
  message.clear = color.green;
  message.clear = color.blue;

  message.gain = getGain();
  message.integration_cycles = getIntegrationCycles();

  sender->put(message);
}

bool RaspiColorNode::getEnabled() {
  return i2c_smbus_read_byte_data(file_descriptor, enable) == on;
}

bool RaspiColorNode::getValid() {
  return i2c_smbus_read_byte_data(file_descriptor, status) & avalid;
}

uint8_t RaspiColorNode::getGain() {
  return i2c_smbus_read_byte_data(file_descriptor, control);
}

uint8_t RaspiColorNode::getIntegrationCycles() {
  return ~i2c_smbus_read_byte_data(file_descriptor, atime);
}

RaspiColorNode::RawColor RaspiColorNode::getColor() {
  RawColor result;

  const int32_t size = i2c_smbus_read_i2c_block_data(file_descriptor, cdata, sizeof(result), reinterpret_cast<uint8_t *>(&result));
  if (size < 0) {
    throw lbot::IoException("Failed to read color data.", -size);
  }
  if (size != 8) {
    throw lbot::IoException("Failed to read color data (expecting 8 bytes, got " + std::to_string(size) + ").");
  }

  result.clear = le16toh(result.clear);
  result.red = le16toh(result.red);
  result.green = le16toh(result.green);
  result.blue = le16toh(result.blue);

  return result;
}

void RaspiColorNode::setEnabled(bool status) {
  if (status) {
    i2c_smbus_write_byte_data(file_descriptor, enable, pon);
    lbot::Thread::sleepFor(std::chrono::microseconds(2400));
    i2c_smbus_write_byte_data(file_descriptor, enable, on);
  } else {
    i2c_smbus_write_byte_data(file_descriptor, enable, off);
  }
}

void RaspiColorNode::setGain(uint8_t gain) {
  i2c_smbus_write_byte_data(file_descriptor, control, gain);
}

void RaspiColorNode::setIntegrationCycles(uint8_t integration_cycles) {
  i2c_smbus_write_byte_data(file_descriptor, atime, ~integration_cycles);
}

}}
