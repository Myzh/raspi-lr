#pragma once

#include <labrat/lbot/plugin.hpp>
#include <labrat/lbot/utils/thread.hpp>


inline namespace labrat {
namespace example_raspi {

class RaspiSenseHatPlugin : public lbot::Plugin {
public:
  RaspiSenseHatPlugin();

private:
  uint64_t time = 0;

  lbot::TimerThread thread;
};

}}
