#include <labrat/example-raspi/raspi-sense-hat/joystick_node.hpp>
#include <labrat/lbot/exception.hpp>

#include <chrono>

#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/epoll.h>
#include <linux/input.h>
#include <unistd.h>

inline namespace labrat {
namespace example_raspi {

RaspiJoystickNode::RaspiJoystickNode(){
  sender = addSender<JoystickInputMessage>("/raspi_sense_hat/joystick");

  file_descriptor = openJoystick();

  epoll_handle = epoll_create(1);

  epoll_event event;
  event.events = EPOLLIN;

  if (epoll_ctl(epoll_handle, EPOLL_CTL_ADD, file_descriptor, &event) != 0) {
    throw lbot::IoException("Failed to create epoll instance.", errno);
  }

  sigemptyset(&signal_mask);
  sigaddset(&signal_mask, SIGINT);

  thread = lbot::LoopThread(&RaspiJoystickNode::readJoystick, "raspi joystick", 1, this);
}

RaspiJoystickNode::~RaspiJoystickNode() {
  close(epoll_handle);
  close(file_descriptor);
}

std::size_t RaspiJoystickNode::openJoystick() {
  for (std::size_t i = 0; i < max_files; ++i) {
    const std::string filename = "/dev/input/event" + std::to_string(i);

    std::size_t result = open(filename.c_str(), O_RDWR);

    if (result < 0) {
      if (errno == ENOENT) {
        throw lbot::IoException("No joystick found");
      }
    }

    std::array<char, 256> name;
    ioctl(result, EVIOCGNAME(name.size()), name.data());

    if (std::string(name.data()) == "Raspberry Pi Sense HAT Joystick") {
      return result;
    }

    close(result);
  }

  throw lbot::IoException("No joystick found within allowed index range");
}

void RaspiJoystickNode::readJoystick() {
  {
    epoll_event event;
    const int32_t result = epoll_pwait(epoll_handle, &event, 1, timeout, &signal_mask);

    if (result <= 0) {
      if ((result == -1) && (errno != EINTR)) {
        throw lbot::IoException("Failure during epoll wait.", errno);
      }

      sender->put(KeyCode::none);

      return;
    }
  }

  std::array<input_event, 64> events;
  const std::size_t size = ::read(file_descriptor, reinterpret_cast<void *>(events.data()), events.size() * sizeof(input_event));

  if (size < 0) {
    throw lbot::IoException("Failed to read from joystick input device.", errno);
  }

  if (size < sizeof(input_event)) {
    throw lbot::IoException("Failed to read from joystick input device (expecting " + std::to_string(sizeof(input_event)) + " bytes, got " + std::to_string(size) + ").");
  }

  for (std::size_t i = 0; i < size / sizeof(input_event); ++i) {
    if (events[i].type == EV_KEY) {
      sender->put(static_cast<KeyCode>(events[i].code));
    }
  }
}

}}
