#pragma once

#include <labrat/lbot/message.hpp>
#include <labrat/lbot/node.hpp>
#include <labrat/lbot/utils/thread.hpp>
#include <labrat/example/msg/color.hpp>

inline namespace labrat {
namespace example_raspi {

class RaspiColorNode : public lbot::Node {
public:
  /**
   * @brief Construct a new Raspi Sense Hat Node object
   *
   */
  RaspiColorNode();
  ~RaspiColorNode();

private:
  struct RawColor {
    uint16_t clear;
    uint16_t red;
    uint16_t green;
    uint16_t blue;
  };

  static std::size_t openColorSensor();

  void readColor();

  bool getEnabled();
  bool getValid();
  uint8_t getGain();
  uint8_t getIntegrationCycles();
  RawColor getColor();
  
  void setEnabled(bool status);
  void setGain(uint8_t gain);
  void setIntegrationCycles(uint8_t integration_cycles);

  Sender<example::msg::Color>::Ptr sender;
  
  lbot::TimerThread thread;

  std::size_t file_descriptor;

  static constexpr uint8_t command_bit = 0x80;
  static constexpr uint8_t enable = 0x00 | command_bit;
  static constexpr uint8_t atime = 0x01 | command_bit;
  static constexpr uint8_t control = 0x0f | command_bit;
  static constexpr uint8_t id = 0x12 | command_bit;
  static constexpr uint8_t status = 0x13 | command_bit;
  static constexpr uint8_t cdata = 0x14 | command_bit;
  static constexpr uint8_t rdata = 0x16 | command_bit;
  static constexpr uint8_t gdata = 0x18 | command_bit;
  static constexpr uint8_t bdata = 0x1a | command_bit;
  static constexpr uint8_t off = 0x00;
  static constexpr uint8_t pon = 0x01;
  static constexpr uint8_t aen = 0x02;
  static constexpr uint8_t on = pon | aen;
  static constexpr uint8_t avalid = 0x01;

  static constexpr std::size_t max_files = 100;
  static constexpr std::size_t timeout = 1000;
};

}}
