#include <labrat/example-raspi/raspi-sense-hat/plugin.hpp>
#include <labrat/example-raspi/raspi-sense-hat/color_node.hpp>
#include <labrat/example-raspi/raspi-sense-hat/imu_node.hpp>
#include <labrat/example-raspi/raspi-sense-hat/joystick_node.hpp>
#include <labrat/example-raspi/raspi-sense-hat/led_node.hpp>


inline namespace labrat {
namespace example_raspi {

RaspiSenseHatPlugin::RaspiSenseHatPlugin() {
  addNode<RaspiColorNode>("raspi_color");
  addNode<RaspiImuNode>("raspi_imu");
  addNode<RaspiJoystickNode>("raspi_joystick");
  std::shared_ptr<RaspiLedNode> led_node(addNode<RaspiLedNode>("raspi_led"));

  thread = lbot::TimerThread([this, led_node]() {
    if (!led_node->framebufferReady()) {
      return;
    }

    for (uint64_t i = 0; i < 8; ++i) {
      for (uint64_t j = 0; j < 8; ++j) {
        example_raspi::FramebufferPixel &pixel = led_node->getPixel(i, j);

        const uint64_t index = time + i + j;
        const uint8_t mode = (index >> 5) % 3;

        switch (mode) {
          case (0): {
            pixel.red = index;
            pixel.green = ~index << 1;
            pixel.blue = 0;
            break;
          }
          case (1): {
            pixel.red = ~index;
            pixel.green = 0;
            pixel.blue = index;
            break;
          }
          case (2): {
            pixel.red = 0;
            pixel.green = index << 1;
            pixel.blue = ~index;
            break;
          }
        }
      }
    }

    ++time;
  }, std::chrono::milliseconds(20), "led control", 1);
}

}}
