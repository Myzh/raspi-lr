#pragma once

#include <labrat/example-raspi/calibration/conversions.hpp>

#include <labrat/lbot/node.hpp>
#include <labrat/lbot/utils/thread.hpp>

#include <vector>

inline namespace labrat {
namespace example_raspi {

class CalibrationNode : public lbot::Node {
public:
  /**
   * @brief Construct a new Camera Node object
   *
   */
  CalibrationNode();
  ~CalibrationNode();

private:
  void run();

  Sender<CalibrationParametersMessage>::Ptr sender_parameters;
  Sender<CalibrationAnnotationsMessage>::Ptr sender_corners;
  Sender<CalibrationImageMessage>::Ptr sender_image;

  Receiver<CalibrationImageMessage>::Ptr receiver_image;

  bool first_flag;
  std::vector<cv::Mat2f> image_points;
  std::vector<std::vector<cv::Point3f>> object_points;
  CalibrationParameters parameters;

  lbot::LoopThread thread;
};

}}
