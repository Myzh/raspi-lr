#include <labrat/example-raspi/calibration/conversions.hpp>

#include <chrono>

#include <labrat/lbot/exception.hpp>

inline namespace labrat {
namespace example_raspi {

void CalibrationImageMessage::convertFrom(const cv::Mat1b &source, Storage &destination) {
  destination.timestamp = std::make_unique<foxglove::Time>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count(), (std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()) % std::chrono::seconds(1)).count());

  if (!source.isContinuous()) {
    throw lbot::IoException("Image matrix is not continuous.");
  }

  destination.frame_id = "enu";
  destination.encoding = "mono8";

  destination.width = source.size().width;
  destination.height = source.size().height;
  destination.step = source.step;

  const int size = source.dataend - source.data;
  destination.data.resize(size);
  memcpy(destination.data.data(), source.data, size);
}

void CalibrationImageMessage::convertTo(const Storage &source, cv::Mat1b &destination) {
  if (source.encoding != "yuyv") {
    throw lbot::IoException("Frame encoding unsupported.");
  }

  cv::Mat image = cv::Mat1b(source.data, true).reshape(2, source.height);
  cv::cvtColor(image, destination, cv::COLOR_YUV2GRAY_YUYV);
}

void CalibrationImageMessage::moveTo(Storage &&source, cv::Mat1b &destination) {
  if (source.encoding != "yuyv") {
    throw lbot::IoException("Frame encoding unsupported.");
  }

  cv::Mat image = cv::Mat1b(source.data, false).reshape(2, source.height);
  cv::cvtColor(image, destination, cv::COLOR_YUV2GRAY_YUYV);
}

void CalibrationParametersMessage::convertFrom(const CalibrationParameters &source, Storage &destination) {
  destination.timestamp = std::make_unique<foxglove::Time>(std::chrono::duration_cast<std::chrono::seconds>(lbot::Clock::now().time_since_epoch()).count(), (std::chrono::duration_cast<std::chrono::nanoseconds>(lbot::Clock::now().time_since_epoch()) % std::chrono::seconds(1)).count());
  destination.frame_id = "enu";
  destination.distortion_model = "plumb_bob";

  destination.width = source.size.width;
  destination.height = source.size.height;

  {
    const int size = source.distortion.size().width;

    destination.d.clear();
    destination.d.reserve(size);

    for (int i = 0; i < size; ++i) {
      destination.d.emplace_back(source.distortion.at<double>(0, i));
    }
  }

  {
    destination.k.clear();
    destination.k.reserve(9);
    destination.p.clear();
    destination.p.reserve(12);

    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) {
        destination.k.emplace_back(source.camera.at<double>(i, j));
        destination.p.emplace_back(source.camera.at<double>(i, j));
      }
      destination.p.emplace_back(0);
    }
  }

  destination.r = {1, 0, 0, 0, 1, 0, 0, 0, 1};
}

void CalibrationAnnotationsMessage::convertFrom(const cv::Mat2f &source, Storage &destination) {
  const foxglove::Time timestamp(std::chrono::duration_cast<std::chrono::seconds>(lbot::Clock::now().time_since_epoch()).count(), (std::chrono::duration_cast<std::chrono::nanoseconds>(lbot::Clock::now().time_since_epoch()) % std::chrono::seconds(1)).count());

  destination.points.clear();

  const foxglove::ColorNative color{.r = 0xff, .g = 0x00, .b = 0x00, .a = 0x80};
  
  std::unique_ptr<foxglove::PointsAnnotationNative> &annotation = destination.points.emplace_back(std::make_unique<foxglove::PointsAnnotationNative>());

  annotation->timestamp = std::make_unique<foxglove::Time>(timestamp);
  annotation->type = foxglove::PointsAnnotationType::LINE_STRIP;

  annotation->points.clear();
  annotation->points.reserve(source.rows);
  annotation->outline_colors.clear();
  annotation->outline_colors.reserve(source.rows);

  for (int i = 0; i < source.rows; ++i) {
    std::unique_ptr<foxglove::Point2Native> &point = annotation->points.emplace_back(std::make_unique<foxglove::Point2Native>());
    annotation->outline_colors.push_back(std::make_unique<foxglove::ColorNative>(color));

    point->x = source.at<cv::Vec2f>(i)[0];
    point->y = source.at<cv::Vec2f>(i)[1];
  }

  annotation->outline_colors.pop_back();
  annotation->thickness = 2;
}

}}
