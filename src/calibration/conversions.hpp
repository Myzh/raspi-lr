#pragma once

#include <labrat/lbot/message.hpp>
#include <labrat/lbot/msg/foxglove/RawImage.hpp>
#include <labrat/lbot/msg/foxglove/CameraCalibration.hpp>
#include <labrat/lbot/msg/foxglove/ImageAnnotations.hpp>

#include <opencv2/opencv.hpp>

inline namespace labrat {
namespace example_raspi {

struct CalibrationParameters {
  cv::Mat1d camera;
  cv::Mat1d distortion;
  cv::Size size;
};

class CalibrationImageMessage : public lbot::MessageBase<foxglove::RawImage, cv::Mat1b> {
public:
  static void convertFrom(const cv::Mat1b &source, Storage &destination);
  static void convertTo(const Storage &source, cv::Mat1b &destination);
  static void moveTo(Storage &&source, cv::Mat1b &destination);
};

class CalibrationParametersMessage : public lbot::MessageBase<foxglove::CameraCalibration, CalibrationParameters> {
public:
  static void convertFrom(const CalibrationParameters &source, Storage &destination);
};

class CalibrationAnnotationsMessage : public lbot::MessageBase<foxglove::ImageAnnotations, cv::Mat2f> {
public:
  static void convertFrom(const cv::Mat2f &source, Storage &destination);
};

}}
