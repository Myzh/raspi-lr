#include <labrat/example-raspi/calibration/node.hpp>

#include <chrono>

#include <labrat/lbot/config.hpp>
#include <labrat/lbot/exception.hpp>

#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>

inline namespace labrat {
namespace example_raspi {

CalibrationNode::CalibrationNode(){
  sender_parameters = addSender<CalibrationParametersMessage>("/calibration/parameters");
  sender_corners = addSender<CalibrationAnnotationsMessage>("/calibration/corners");
  sender_image = addSender<CalibrationImageMessage>("/calibration/image");

  receiver_image = addReceiver<CalibrationImageMessage>("/camera/raw");

  first_flag = true;

  thread = lbot::LoopThread(&CalibrationNode::run, "calib", 1,  this);
}

CalibrationNode::~CalibrationNode() = default;

void CalibrationNode::run() {
  lbot::Config::Ptr config = lbot::Config::get();
  cv::Mat1b image;

  try {
    image = receiver_image->latest();
  } catch (lbot::TopicNoDataAvailableException &) {
    lbot::Thread::sleepFor(std::chrono::seconds(1));
    return;
  }

  getLogger().logInfo() << "Processing image...";

  const cv::Size pattern_size(config->getParameterFallback("/calibration/pattern_x", 3).get<int>(), config->getParameterFallback("/calibration/pattern_y", 7).get<int>());
  cv::Mat2f corners;

  if (!cv::findChessboardCorners(image, pattern_size, corners)) {
    getLogger().logWarning() << "Failed to find chessboard corners";
    return;
  }

  cv::cornerSubPix(image, corners, cv::Size(11, 11), cv::Size(-1, -1), cv::TermCriteria(cv::TermCriteria::COUNT | cv::TermCriteria::EPS, 30, 0.001));
 
  getLogger().logDebug() << "Detected " << corners.rows << "x" << corners.cols << " corners";

  sender_image->put(image);
  sender_corners->put(corners);
  image_points.emplace_back(std::move(corners));

  std::vector<cv::Point3f> object_coords(pattern_size.width * pattern_size.height);
  for (int i = 0; i < pattern_size.height; i++) {
    for (int j = 0; j < pattern_size.width; j++) {
      object_coords[i * pattern_size.width + j] = cv::Point3f(j, i, 0);
    }
  }
  object_points.emplace_back(std::move(object_coords));

  int flags = cv::CALIB_RATIONAL_MODEL | cv::CALIB_FIX_PRINCIPAL_POINT;

  if (first_flag) {
    parameters.size = cv::Size(image.cols, image.rows);
    first_flag = false;
  } else {
    flags |= cv::CALIB_USE_INTRINSIC_GUESS;
  }

  std::vector<cv::Mat> rvecs;
  std::vector<cv::Mat> tvecs;
  double result = cv::calibrateCamera(object_points, image_points, parameters.size, parameters.camera, parameters.distortion, rvecs, tvecs, flags);

  sender_parameters->put(parameters);
  getLogger().logInfo() << "RMS:" << result;
}

}}
