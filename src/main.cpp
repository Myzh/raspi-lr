#include <labrat/lbot/logger.hpp>
#include <labrat/lbot/manager.hpp>
#include <labrat/lbot/filter.hpp>
#include <labrat/lbot/config.hpp>
#include <labrat/lbot/plugins/linux/stats.hpp>
#include <labrat/lbot/plugins/foxglove-ws/server.hpp>
#include <labrat/lbot/plugins/mcap/recorder.hpp>
#include <labrat/example-raspi/raspi-sense-hat/plugin.hpp>
#include <labrat/example-raspi/calibration/node.hpp>
#include <labrat/example-raspi/camera/node.hpp>
#include <labrat/example-raspi/encoder/node.hpp>
#include <labrat/example-raspi/aruco/plugin.hpp>
#include <labrat/example-raspi/scene/node.hpp>
#include <clipp.h>

#include <iostream>

#include <signal.h>

int main(int argc, char **argv) {
  bool verbose = false;
  bool help = false;
  std::string config_file = "config.yaml";

  struct NodeActivation {
    bool calibration = false;
  } node_activation;

  struct NodeDeactivation {
    bool sense_hat = false;
    bool camera = false;
    bool encoder = false;
    bool aruco = false;
  } node_deactivation;

  auto cli = (
    clipp::option("-v", "--verbose").set(verbose).doc("Verbose output"),
    clipp::option("-h", "--help").set(help).doc("Show this help"),
    clipp::option("-c", "--config").set(config_file).doc("Configuration file."),
    clipp::option("--calibration").set(node_activation.calibration).doc("Enable camera calibration nodes."),
    clipp::option("--no-sense-hat").set(node_deactivation.sense_hat).doc("Disable Raspi Sense HAT nodes."),
    clipp::option("--no-camera").set(node_deactivation.camera).doc("Disable Camera nodes."),
    clipp::option("--no-encoder").set(node_deactivation.encoder).doc("Disable Encoder nodes."),
    clipp::option("--no-aruco").set(node_deactivation.aruco).doc("Disable Aruco nodes.")
  );

  clipp::parse(argc, argv, cli);

  if (help) {
    std::cout << "Usage:" << std::endl;
    std::cout << clipp::usage_lines(cli, "example-raspi") << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << clipp::documentation(cli) << std::endl;

    return 0;
  }

  lbot::Manager::Ptr manager = lbot::Manager::get();

  lbot::Logger logger("main");
  lbot::Logger::setLogLevel(verbose ? lbot::Logger::Verbosity::debug : lbot::Logger::Verbosity::info);
  logger.logInfo() << "Starting application.";

  lbot::Config::Ptr config = lbot::Config::get();
  try {
    config->load(config_file);
  } catch (lbot::ConfigParseException &) {
    logger.logWarning() << "Failed to parse config.";
  }

  lbot::Filter filter;
  filter.blacklist("/camera/raw");

  manager->addPlugin<lbot::plugins::McapRecorder>("mcap", filter);
  logger.logInfo() << "Recording started.";

  manager->addPlugin<lbot::plugins::FoxgloveServer>("foxglove-ws", filter);
  logger.logInfo() << "Server started.";

  manager->addPlugin<lbot::plugins::LinuxStats>("stats");
  logger.logInfo() << "Linux stats started.";

  if (!node_deactivation.sense_hat) {
    manager->addPlugin<example_raspi::RaspiSenseHatPlugin>("sense_hat");
    logger.logInfo() << "Raspi Sense HAT plugin nodes connected.";

    manager->addNode<example_raspi::SceneNode>("scene");
    logger.logInfo() << "Scene node connected.";
  }

  if (!node_deactivation.camera) {
    manager->addNode<example_raspi::CameraNode>("camera");
    logger.logInfo() << "Camera node connected.";
  }

  if (!node_deactivation.encoder) {
    manager->addNode<example_raspi::EncoderNode>("encoder");
    logger.logInfo() << "Encoder node connected.";
  }

  if (node_activation.calibration) {
    manager->addNode<example_raspi::CalibrationNode>("calib");
    logger.logInfo() << "Calibration node connected.";
  }

  if (!node_deactivation.aruco) {
    manager->addPlugin<example_raspi::ArucoPlugin>("aruco");
    logger.logInfo() << "Aruco plugin nodes connected.";
  }

  sigset_t signal_mask;
  sigemptyset(&signal_mask);
  sigaddset(&signal_mask, SIGINT);
  sigprocmask(SIG_BLOCK, &signal_mask, NULL);

  int signal;
  sigwait(&signal_mask, &signal);
  logger.logInfo() << "Caught signal (" << signal << "), shutting down.";

  return 0;
}
