from conan import ConanFile
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
from conan.tools.files import copy
from conan.tools.system.package_manager import Apt
import os

class RaspiLbotConan(ConanFile):
    name = "raspi-lbot"
    version = "v0.0.0"
    author = "Max Yvon Zimmermann (maxyvon@gmx.de)"
    description = "Examples for lbot"
    settings = "os", "compiler", "build_type", "arch"
    options = {"with_system_deps": [True, False]}
    default_options = {"with_system_deps": False}
    exports_sources = ("CMakeLists.txt", "src/*")

    def configure(self):
        self.options["opencv"].aruco = True
        self.options["opencv"].neon = True
        self.options["opencv"].with_eigen = True
        self.options["opencv"].with_v4l = True
        self.options["opencv"].with_jpeg = "libjpeg"
        self.options["opencv"].with_ffmpeg = False
        self.options["opencv"].with_gtk = False
        self.options["opencv"].with_openexr = False
        self.options["opencv"].dnn = False

        self.options["libtiff"].libdeflate = False

    def system_requirements(self):
        if self.settings.os != 'Linux':
            raise Exception("Package is only supported on Linux.")

        apt = Apt(self)
        apt.install(["librtimulib-dev"], check = True)

    def requirements(self):
        if self.options.with_system_deps:
            return

        self.requires("lbot/v0.0.12+419069b")
        self.requires("opencv/[>=4.8.1]")
        self.requires("clipp/[>=1.2.3]")
        self.requires("i2c-tools/[>=4.3]")

    def build_requirements(self):
        if self.options.with_system_deps:
            return

        self.tool_requires("lbot/v0.0.12+419069b")
        self.tool_requires("cmake/[>=3.22.0]")

    def layout(self):
        cmake_layout(self)

    def export_sources(self):
        copy(self, "CMakeLists.txt", self.recipe_folder, self.export_sources_folder)
        copy(self, "src/*", self.recipe_folder, self.export_sources_folder)
        copy(self, "cmake/*", self.recipe_folder, self.export_sources_folder)

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()

        toolchain = CMakeToolchain(self)
        toolchain.variables["CMAKE_RUNTIME_OUTPUT_DIRECTORY"] = "${CMAKE_BINARY_DIR}/bin"
        toolchain.variables["CMAKE_LIBRARY_OUTPUT_DIRECTORY"] = "${CMAKE_BINARY_DIR}/lib"
        toolchain.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
