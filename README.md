# Example for labrat-robot with the Raspberry Pi

Just a small example project to make use of cameras, aruco marker detecttion and the Sense HAT sensors.

## Build

> Its is recommended to cross-compile the project alongside all of its dependencies.
> This greatly reduces the build times.

### Apt

This package requires the apt package manager. You may need to add repositories for the arm architetcure on your build machine.
```
sudo dpkg --add-architecture arm64
```

Create a new file `/etc/apt/sources.list.d/raspi-cross-compile.list` with the following contents. You may need to adjust the version and mirror depending on your system.
```
deb [arch=arm64] http://ports.ubuntu.com/ jammy main restricted universe multiverse
deb [arch=arm64] http://ports.ubuntu.com/ jammy-updates main restricted universe multiverse
deb [arch=arm64] http://ports.ubuntu.com/ jammy-security main restricted universe multiverse
deb [arch=arm64] http://ports.ubuntu.com/ jammy-backports main restricted universe multiverse
```
You should also prefix any other repositories on your system with `[arch=amd64]` (may need to adjust architecture).

```
sudo apt update
sudo apt install gcc-aarch64-linux-gnu  g++-aarch64-linux-gnu
```

### Conan

Both machines should have the same version of conan installed. Having a clean cache on both machines can also avoid potential issues.
```
pip install --upgrade conan
conan cache clean
```

On the build machine create a conan profile as `~/.conan2/profiles/raspi` with the following contents.
```
[settings]
arch=armv8
build_type=Release
compiler=gcc
compiler.cppstd=20
compiler.libcxx=libstdc++11
compiler.version=11
os=Linux

[buildenv]
CC=aarch64-linux-gnu-gcc-11
CXX=aarch64-linux-gnu-g++-11
LD=aarch64-linux-gnu-ld
```

Now build the host package on the build machine.
```
PKG_CONFIG_PATH=/usr/lib/aarch64-linux-gnu/pkgconfig conan create . -pr:h=raspi -pr:b=default -b=missing -c tools.system.package_manager:mode=install -c tools.system.package_manager:sudo=true -c tools.system.package_manager:sudo_askpass=true
```

Next we need to create a conan remote to transfer our build to the Raspberry Pi.
Run the following commands in another shell (still on the build machine).
```
sudo pip install conan-server
conan_server
```
You may need to tweak the config file (`.conan_server/server.conf`) in order to allow writes. The default user/password should be "demo"/"demo".

Now add the remote on the build machine and upload the package.
```
conan remote add local http://localhost:9300
conan install . -pr:h=raspi -pr:b=default -b=missing -f=json > conan_build.json
conan list -g=conan_build.json -gb=* -f=json > conan_list.json
conan upload --list=conan_list.json -r=local -cc=core.upload:parallel=`nproc`
```

Switch over to the Raspberry Pi and create a similar conan profile as on the build machine.
```
[settings]
arch=armv8
build_type=Release
compiler=gcc
compiler.cppstd=20
compiler.libcxx=libstdc++11
compiler.version=11
os=Linux
```

Now add the conan remote. The Raspberry Pi and the build machine should be on the same network.
```
conan remote add local http://[build machine IP]:9300
```

Now clone the repository on the host machine.
Afterwards you can build the package.
```
conan build . -pr:h=raspi -pr:b=raspi -b=missing -c tools.system.package_manager:mode=install -c tools.system.package_manager:sudo=true -c tools.system.package_manager:sudo_askpass=true
```

# Setup

## Environment

Enable I2C and legacy camera support on the Raspberry Pi through raspi-config.
```
raspi-config
```

Now reboot the system.

Before running the application, make sure any devices used by the application do have the correct permissions.

## Sensor calibration

You need to calibrate the IMU and provide the camera intrinsic parameters.
For the IMU calibration you can follow this [tutorial](https://www.raspberrypi.com/documentation/accessories/sense-hat.html).
The camera calibration can be perfomed by running the `calibration.py` script.

Sample configurations can be found [here](https://gitlab.com/Myzh/raspi-lr/-/snippets/2518365)
